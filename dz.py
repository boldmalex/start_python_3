words = ['python', 'c++', 'c', 'scala', 'java']


def count_letter(letter):

    count = 0

    for word in words:
        for w_letter in word:
            if w_letter == letter:
                count += 1

    return count


print(f"Набор слов: {words}")
letter = input("Введите букву для поиска: ")
print(f"Кол-во найденных букв {letter} = {count_letter(letter)}")
